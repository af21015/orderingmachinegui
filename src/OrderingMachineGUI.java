import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

public class OrderingMachineGUI extends JFrame {
    private JPanel root;
    private JButton udonButton;
    private JTabbedPane tab;
    private JPanel foodPanel;
    private JPanel drinkPanel;
    private JTextPane textMessage;
    private JButton checkOutButton;
    private JButton pizzaButton;
    private JButton curryButton;
    private JButton tempuraButton;
    private JButton pastaButton;
    private JButton sobaButton;
    private JLabel totalLabel;
    private JButton beerButton;
    private JButton colaButton;
    private JButton orengeButton;
    private JButton lemonButton;
    private JButton melonButton;
    private JButton winButton;
    private JTextPane footer;
    private int sum=0;
    private String currentText;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int day = calendar.get(Calendar.DATE);
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minute = calendar.get(Calendar.MINUTE);
    int second = calendar.get(Calendar.SECOND);
    int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;

    void order(String foodName, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "You ordered " + foodName + "!");
            currentText = textMessage.getText();
            textMessage.setText(currentText + foodName + " "+price+"yen" + "\n");
            sum += price;
            totalLabel.setText("Total "+sum+"yen");
        }
    }


    public OrderingMachineGUI() {

        footer.setText(
                "Note\n" +
                "・Alcohol service is available from 5:00 PM until 5:00 AM the following morning.\n"
        );
        footer.setEditable(false);

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",450);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",250);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",350);
            }
        });
        pizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza",500);
            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry",300);
            }
        });
        pastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pasta",400);
            }
        });
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cola",100);
            }
        });
        orengeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Orenge Juice",150);
            }
        });
        melonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Melon Soda",130);
            }
        });

        if(hour>=17 || (hour>=0 && hour<5)) {
            beerButton.setIcon(new ImageIcon(
                    this.getClass().getResource("beer.png")
            ));
            beerButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Beer",200);
                }
            });
            lemonButton.setIcon(new ImageIcon(
                    this.getClass().getResource("lemon.jpg")
            ));
            lemonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Lemon Sour",190);
                }
            });
            winButton.setIcon(new ImageIcon(
                    this.getClass().getResource("wine.jpg")
            ));
            winButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Wine",210);
                }
            });
        }
        else {
            beerButton.setEnabled(false);
            lemonButton.setEnabled(false);
            winButton.setEnabled(false);
        }

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total price is " + sum + "yen!");
                    //CheckOut後、全て初期化
                    sum = 0;
                    totalLabel.setText("Total 0yen");
                    textMessage.setText("");
                    currentText = "";
                }
            }
        });

        //画像追加
        pizzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("pizza.jpg")
        ));
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("soba.jpg")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));
        pastaButton.setIcon(new ImageIcon(
                this.getClass().getResource("pasta.jpg")
        ));
        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));
        orengeButton.setIcon(new ImageIcon(
                this.getClass().getResource("orange.jpg")
        ));
        melonButton.setIcon(new ImageIcon(
                this.getClass().getResource("melon.jpg")
        ));
        colaButton.setIcon(new ImageIcon(
                this.getClass().getResource("cola.jpg")
        ));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderingMachineGUI");
        frame.setContentPane(new OrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
